# USB AutoRun

An app for Windows users aiming to restore Autorun functionalities for USB devices (such as launching an app from the USB device), but with great control over what exactly happens.

## Installation

Download the latest release from the [Releases](https://gitlab.com/Moravuscz/usb-autorun/-/releases) page. You can choose an installer or a zipped version.

## Contributing
Pull requests are (somewhat) welcome. For major changes, please open an issue first to discuss what you would like to change.

If you wish to add more functionality you can make a plugin for that and submit a pull request into [USB AutoRun Plugins](https://gitlab.com/Moravuscz/usb-autorun-plugins) repository.