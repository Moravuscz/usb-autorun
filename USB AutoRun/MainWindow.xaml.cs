﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using iTuner;
using INIFiles;


namespace USB_AutoRun
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly string CR = Environment.NewLine;
        private static UsbManager manager = new UsbManager();
        private UsbDiskCollection disks = manager.GetAvailableDisks();
        INIFile inifile = new INIFile("F:\\Autorun.ini");
        public MainWindow()
        {
            InitializeComponent();
            if (disks.Count > 0) { FillDiskListView(disks); }
            manager.StateChanged += new UsbStateChangedEventHandler(DoStateChanged);
            
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e) { this.Close(); }
        
        /// <summary>
        /// Actions to take when the state changes
        /// </summary>
        /// <param name="e"></param>
        private void DoStateChanged(UsbStateChangedEventArgs e)
        {
            switch (e.State)
            {
                case UsbStateChange.Added:
                {
                    disks.Add(e.Disk);
                    break;
                }
                case UsbStateChange.Removed:
                {
                    disks.Remove(e.Disk.Name);
                    break;
                }
            }
            FillDiskListView(disks);
        }

        /// <summary>
        /// Fills the ListView on the left with a list of available drives
        /// </summary>
        /// <param name="disks">A collection of disks to use</param>
        private void FillDiskListView(UsbDiskCollection disks)
        {
            DriveList.Items.Clear();
            foreach (UsbDisk disk in disks)
            {
                DriveList.Items.Add(new System.Windows.Controls.ListViewItem() { Content = disk.ToString() });
            }
        }

        private void DriveList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UsbDisk disk = disks[disks.IndexOf(e.AddedItems[0].ToString().Substring(e.AddedItems[0].ToString().IndexOf(": ") + 2, 2))];
            StatusBar.Content = inifile.GetValue("Section", "Key");
        }
    }
}