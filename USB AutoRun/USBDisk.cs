﻿//************************************************************************************************
// Copyright © 2010 Steven M. Cohn. All Rights Reserved.
//
//************************************************************************************************

namespace iTuner
{
	using System;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;


    /// <summary>
    /// Represents the displayable information for a single USB disk.
    /// </summary>
    public class UsbDisk
	{
		private const int KB = 1024;
		private const int MB = KB * 1000;
		private const int GB = MB * 1000;


		/// <summary>
		/// Initialize a new instance with the given values.
		/// </summary>
		/// <param name="name">The Windows drive letter assigned to this device.</param>
		internal UsbDisk (string name)
		{
			this.Name = name;
			this.Model = String.Empty;
			this.Volume = String.Empty;
            this.VolumeSerial = String.Empty;
			this.FreeSpace = 0;
			this.Size = 0;
            this.DeviceSerial = String.Empty;
            this.Eligible = false;
		}


		/// <summary>
		/// Gets the available free space on the disk, specified in bytes.
		/// </summary>
		public ulong FreeSpace
		{
			get;
			internal set;
		}


		/// <summary>
		/// Get the model of this disk. This is the manufacturer's name.
		/// </summary>
		/// <remarks>
		/// When this class is used to identify a removed USB device, the Model
		/// property is set to String.Empty.
		/// </remarks>
		public string Model
		{
			get;
			internal set;
		}


		/// <summary>
		/// Gets the name of this disk. This is the Windows identifier, drive letter.
		/// </summary>
		public string Name
		{
			get;
			private set;
		}


		/// <summary>
		/// Gets the total size of the disk, specified in bytes.
		/// </summary>
		public ulong Size
		{
			get;
			internal set;
		}


		/// <summary>
		/// Get the volume name of this disk. This is the friendly name ("Stick").
		/// </summary>
		/// <remarks>
		/// When this class is used to identify a removed USB device, the Volume
		/// property is set to String.Empty.
		/// </remarks>
		public string Volume
		{
			get;
			internal set;
		}

        /// <summary>
        /// Get the volume serial number of this disk.
        /// </summary>
        public string VolumeSerial
        {
            get;
            internal set;
        }

        /// <summary>
        /// Get the device serial number of this disk. This may or may not be defined by the manufacturer.
        /// </summary>
        /// <remarks>
        /// If there's no serial number input by manufacturer, Windows generates a pseudo-random serial for enumeration purposes that will change every time you plug in the drive.
        /// </remarks>
        public string DeviceSerial
        {
            get;
            internal set;
        }

        /// <summary>
		/// Determines if the drive is eligible to be used with this tool in its full security mode.
		/// </summary>
        /// <remarks>
        /// Requires the drive to have an unique serial number defined by the manufacturer.
        /// </remarks>
        public bool Eligible
        {
            get;
            internal set;
        }


        /// <summary>
        /// Pretty print the disk.
        /// </summary>
        /// <returns></returns>
        public override string ToString ()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(Name);
			builder.Append("\\ ");
			builder.Append(Volume);
			builder.Append(" [");
            builder.Append(VolumeSerial);
			builder.Append("] ");
            builder.Append(Model);
            builder.Append("; SN: ");
            builder.Append(DeviceSerial);
			builder.Append("; Eligible: ");
            builder.Append(Eligible.ToString());

			return builder.ToString();
		}


		private string FormatByteCount (ulong bytes)
		{
			string format = null;

			if (bytes < KB)
			{
				format = String.Format("{0} Bytes", bytes);
			}
			else if (bytes < MB)
			{
				bytes = bytes / KB;
				format = String.Format("{0} KB", bytes.ToString("N"));
			}
			else if (bytes < GB)
			{
				double dree = bytes / MB;
				format = String.Format("{0} MB", dree.ToString("N1"));
			}
			else
			{
				double gree = bytes / GB;
				format = String.Format("{0} GB", gree.ToString("N1"));
			}

            return format;
		}
    }
}
